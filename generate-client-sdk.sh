#!/bin/sh
#
# An example hook script to verify what is about to be committed.
# [snipped much of what used to be in it, added this --
#  make sure you take out the exec of git diff-index!]
npx openapi-generator generate -i api.yaml -g java -o ./client-sdk/java/client -c ./client-sdk/java/config.json